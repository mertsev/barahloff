var mongoose = require('../lib/mongoose'),
    Schema = mongoose.Schema;
var Lot  = require('../models/lot').Lot;
var Bet = require('../models/bet').Bet;
var User = require('../models/user').User;
var Category = require('../models/category').Category;
var pagination = require('mongoose-pagination');
var cloudinary = require('cloudinary');
var async = require("async");
var schedule = require('node-schedule');
var moment = require('moment');

exports.addLot = function Add(req, res) {

        var lot = new Lot({
            title: req.body.title,
            description: req.body.description,
            price: req.body.price,
            _creator: req.user._id,
            _category: req.body.category,
            date: req.body.date,
        });

        var resultArr = [];
        async.each(req.body.photo,
        function (photo, callback) {
            cloudinary.uploader.upload(photo, function (result) {
                resultArr.push(result.url)
                callback();
            });
        },
            function (err) {
             if (err) console.log(err);
            lot.photo = resultArr;
                lot.save(function (err) {
                    if (err) throw err;
                })
                res.json({message: 'Lot added successfully', id: lot._id});

            }
        );

        Category.findByIdAndUpdate(
            req.body.category, {
                $push: {"lots": lot._id}
            },
            {"new": true},
            function (err, category) {
                if (err) return res.send(err);
            });
};


    exports.deleteLot = function Delete(req, res) {
        Lot.remove({
            _id: req.body.lot_id
        }, function (err) {
            if (err) return res.send(err);

            res.json({message: 'Lot deleted successfully'});
        });
    };

    exports.updateLot = function Update(req, res) {
        Lot.findById(req.params.lotId, function (err, lot) {
            if (err) return res.send(err);

            if (req.body.title == null) {
            } else {
                lot.title = req.body.title;
            }

            if (req.body.description == null) {
            } else {
                lot.description = req.body.description;
            }

            if (req.body.price == null) {
            } else {
                lot.price = req.body.price;
            }

            if (req.body.date == null) {
            } else {
                lot.date = req.body.date;
            }


            lot.save(function (err) {
                if (err) return res.send(err);

                res.json({message: 'Lot updated successfully'});
            });
        })
    };

    exports.addBet = function Add(req, res) {

        if (req.body.amount <= req.user.balance) {

            var bet = new Bet({
                userId: req.user._id,
                amount: req.body.amount,
                lot: req.body.lotid
            });

            Lot.findByIdAndUpdate(
                req.body.lotid, {
                    $push: {"bets": bet._id}
                },
                {"new": true},
                function (err, category) {
                    if (err) return console.log(err);
                });
        }

        var contribAmount = 0;
        bet.save({$push: bet},
            function (err) {
                if (err) throw err;
                res.json({message: 'Bet added successfully'});
                function getCurrentContributions(lotId) {
                    var query = Bet.find({ 'lot': lotId});
                    return query;

                }

                var query = getCurrentContributions(req.body.lotid);
                query.exec(function (err, contribs) {
                    if(err) return console.log(err);
                    contribs.forEach(function (contrib) {
                        contribAmount += parseInt(contrib.amount);
                        Lot.findById(req.body.lotid, function (err, lot) {
                            if (err) return handleError(err);

                            lot.currentContribution = parseInt(contribAmount);
                            if (lot.currentContribution >= lot.price) lot.enoughContributed = true;
                            lot.save(function (err, updatedLot) {
                                if (err) return handleError(err);
                            });
                        });
                    })
                })
            });


        var updateUser = new function () {
            User.findById(req.user._id,
                function (err, user) {
                    user.balance = parseInt(user.balance) - parseInt(req.body.amount);
                    if (err) console.log(err);
                    user.save(function (err) {
                        if (err) return console.log(err);
                    })
                })
        };
        updateUser;
    };

exports.lotsPaged = function (req, res) {
    Lot.find()
        .paginate(req.params.page, 100)
        .exec(function (err, docs) {
                res.json(docs)
            }
        );
};

exports.lotsFiltered = function (req, res) {
    Lot
        .find({$text: {$search: req.body.title}})
        .where('enoughContributed').eq(req.body.enoughContributed)
        .where('_category').eq(req.body.category)
        //.where('date').lte(new Date())
        .exec(function (err, docs) {
                res.json(docs)
            }
        );
};


exports.getLot = function (req, res) {
    Lot.findById(req.params.lotId, function (err, user) {

        if (err) return res.send(err);

        res.json(user);
    });
};

exports.userBetAmount = function (req, res, err) {
    var betAmount = 0;
    function getUserBets(lotId, userId) {
        var query = Bet.find({ 'lot': lotId, 'userId': userId});
        return query;
    }

    var query = getUserBets(req.body.lotid, req.user._id);
    query.exec(function (err, bets) {
        if(err) return console.log(err);
        bets.forEach(function (bet) {
            betAmount += parseInt(bet.amount);
        })
        res.json(betAmount)
    })
};

exports.userLots = function (req, res, err) {

    function getUserLots(userId) {
        var query = Lot.find({ '_creator': userId });
        return query;
    }

    var query = getUserLots(req.params.userid);
    query.exec(function (err, lots) {
        var convertedJSON = JSON.parse(JSON.stringify(lots));
        for (i = 0; i < convertedJSON.length; i++){
            delete convertedJSON[i].bets;
        }
        if(err) return console.log(err);
        res.json(convertedJSON);
    })
};

exports.userBets = function (req, res, err) {

    function getUserBets(userId) {
        var query = Bet.find({ 'userId': userId }).distinct('lot');
        return query;
    }

    var query = getUserBets(req.user._id);
    query.exec(function (err, bets) {

        res.json(bets);
    })
};

var rule = new schedule.RecurrenceRule();

rule.minute = new schedule.Range(0, 59, 1);

schedule.scheduleJob(rule, function() {
    function findUnwonLots() {
        date = new Date;
        var query = Lot.find({_winner: null, date: {$lte: date}});
        return query;
    }

    var query = findUnwonLots();
    query.exec(function (req, lots, err) {
        if (err) console.log(err)
        var probability = [];
        var checkpoints = [];
        lots.forEach(function (lot) {
            var betAmount = 0;

            function getLotBets(lot) {
                var query1 = Bet.find({'lot': lot});
                return query1;
            }

            var query1 = getLotBets(lot._id);
            query1
                .sort({amount: 1})
                .exec(function (err, bets) {
                    if (err) return console.log(err);
                    bets.forEach(function (bet) {
                        betAmount += parseInt(bet.amount);
                    })
                    console.log(betAmount);
                    bets.forEach(function (bet) {
                        var prob = bet.amount / betAmount;
                        var user = bet.userId;
                        probability.push({prob, user});
                    })
                    checkpointSet();
                    var randomNumber = Math.random();
                    for (var i = 0; i < checkpoints.length; i++) {
                        if (randomNumber <= checkpoints[i].prob) {
                            console.log("Выйграл " + (checkpoints[i].user));
                            Lot.findById(lot._id, function (err, lot) {
                                if (err) return handleError(err);

                                lot._winner = checkpoints[i].user;
                                lot.save(function (err, updatedLot) {
                                    if (err) return handleError(err);

                                    //console.log(updatedLot);
                                });
                            });
                            break;
                        }
                    }
                })

            var checkpointSet = function () {
                var prob = probability[0].prob;
                var user = probability[0].user;
                checkpoints.push({prob, user});
                for (var i = 1; i < probability.length; i++) {
                    var prob = (checkpoints[i - 1].prob + probability[i].prob);
                    var user = probability[i].user;
                    checkpoints.push({prob, user});
                }
            }
        })
    })
});

exports.lotUsers = function (req, res, err) {
    function findBetUsers() {
        date = new Date;
        var query = Bet.find({lot: req.params.lotid}).distinct('userId');
        return query;
    }
    var json = [];
    var query = findBetUsers();
    query.exec(function(err, bets){
        if (err) console.log(err);
        async.each(bets,
        function (bet, callback) {
            User.findById(bet, function (err, user) {
                json.push({id: user._id, user: user.userName, photo: user.photo});
                callback();
            });
        },
            function (err) {
                res.json(json);
            }
        )
    })
};

exports.userWonLots = function (req, res) {
    var json = [];
    console.log(req.user._id);
    function findWonLots() {
        var query = Lot.find({_winner: req.user._id});
        return query;
    }

    var query = findWonLots();
    query.exec(function (err, lots) {
        if (err) console.log(err);
        async.each(lots,
            function (lot, callback) {
                User.findById(lot._winner, function (err, user) {
                    json.push(lot);
                    callback();
                });
            },
            function (err) {
                res.json(json);
                console.log(json);
            }
        )
    })
    findWonLots();
};
var User = require('../models/user').User;
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var VkontakteStrategy = require('passport-vkontakte').Strategy;

exports.login = function Login(req, res, next) {
    passport.authenticate('local',
        function (err, user, info) {
            return err
                ? next(err)
                : user
                    ? req.logIn(user, function (err) {
                        return err
                            ? next(err)
                            : res.redirect('/');
                    })
                    : res.json(err);
        }
    )(req, res, next);
};

exports.register = function Register(req, res, next) {
    var mail = req.body.email;
    var username = mail.split("@");
    var user = new User({
        email: req.body.email,
        password: req.body.password,
        userName: username[0]
    });
    console.log(user);
    user.save(function (err) {
        return err
            ? next(err)
            : req.logIn(user, function (err) {
                return err
                    ? next(err)
                    : res.redirect('/');
            });
    });
};

exports.logout = function Logout(req, res) {
    req.logout();
    res.redirect('/');
};
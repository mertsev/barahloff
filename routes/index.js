var checkAuth = require('../middleware/checkAuth');

module.exports = function (app) {

    // Authentication REST routing
    app.post  ('/api/register', require('./authentication').register);
    app.post  ('/api/login',    require('./authentication').login);
    app.get   ('/api/logout',   require('./authentication').logout);

    // User data REST routing
    app.get   ('/api/user/:userId', checkAuth, require('./users').getUser);
    app.get   ('/api/user/',        checkAuth, require('./users').getCurrentUser);
    app.put   ('/api/user/',        checkAuth, require('./users').updateUser);
    app.delete('/api/user/',        checkAuth, require('./users').deleteUser);

    // Lot data REST routing
    app.post  ('/api/lot/add',    checkAuth, require('./lotmanagement').addLot);
    app.delete('/api/lot/delete', checkAuth, require('./lotmanagement').deleteLot);
    app.put   ('/api/lot/:lotId', checkAuth, require('./lotmanagement').updateLot);
    app.get   ('/api/lot/:lotId', checkAuth, require('./lotmanagement').getLot);
    app.post  ('/api/lots/',      checkAuth, require('./lotmanagement').addBet);
    app.get   ('/api/lots/:page', checkAuth, require('./lotmanagement').lotsPaged);
    app.post  ('/api/lotsfilter', checkAuth, require('./lotmanagement').lotsFiltered);
    app.post  ('/api/betamount',  checkAuth, require('./lotmanagement').userBetAmount);
    app.get   ('/api/userlots/:userid',   checkAuth, require('./lotmanagement').userLots);
    app.get   ('/api/userbets',   checkAuth, require('./lotmanagement').userBets);
    app.get   ('/api/wonlots',    checkAuth, require('./lotmanagement').userWonLots);
    app.get   ('/api/betters/:lotid', checkAuth, require('./lotmanagement').lotUsers);

    // Category data REST routing
    app.post  ('/api/category',              checkAuth, require('./categories').addCategory);
    app.get   ('/api/category/:categoryId',  checkAuth, require('./categories').getCategory);
    app.put   ('/api/category/:categoryId',  checkAuth, require('./categories').updateCategory);
    app.delete('/api/category/:categoryId',  checkAuth, require('./categories').deleteCategory);
    app.get   ('/api/category',              checkAuth, require('./categories').getAllCategories);

};




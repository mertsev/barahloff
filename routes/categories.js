var Category  = require('../models/category').Category;
var pagination = require('mongoose-pagination');

exports.addCategory = function Add(req, res) {
    console.log(req.body.title);
    var category = new Category({
        title: req.body.title,
        description: req.body.description
    });
    category.save(function (err) {

        if (err) return res.send(err);

        res.json({message: 'Category added successfully'})
    });
};

exports.deleteCategory = function Delete(req, res) {
    Category.remove({
        _id: req.body.categoryId
    }, function (err) {

        if (err) return res.send(err);

        res.json({message: 'Category deleted successfully'});
    });
};

exports.getCategory = function Get(req, res) {
    Category.findById(req.params.categoryId, function (err, category) {
        if (err) return res.send(err);

        res.json(category)
    })
};

exports.updateCategory = function Update(req, res) {
    Category.findById(req.params.categoryId, function (err, category) {

        if (err) return res.send(err);

        category.title = req.body.title;
        category.description = req.body.description;

        category.save(function (err) {

            if (err) return res.send(err);

            res.json({message: 'Category updated successfully'});
        });
    })
};

exports.getAllCategories = function Get(req, res) {
    Category
        .aggregate([{
            $project: { id : 1, title: 1, description: 1}
        }])
        .exec(function (err, category) {

        if (err) return res.send(err);

        res.json(category);
    })
};



var mongoose = require('../lib/mongoose');
var User = require('../models/user').User;
var async = require("async");
var cloudinary = require('cloudinary');

exports.getUser = function find(req, res) {
    User.findById(req.params.userId, function (err, user) {
        if (err) return res.send(err);
        if (req.params.userId == req.user._id) {

            var convertedJSON = JSON.parse(JSON.stringify(user));
            convertedJSON.you = true;
            res.json(convertedJSON);
        } else {
            var convertedJSON = JSON.parse(JSON.stringify(user));
            convertedJSON.you = false;
            res.json(convertedJSON);
        }
    });
};

exports.getCurrentUser = function getUser(req, res) {
    User.findById(req.user._id, function (err, user) {
        if (err) return res.send(err);

        res.json(user)
    });
};

exports.updateUser = function Update(req, res) {
    User.findById(req.user._id, function (err, user) {
        if (err) return res.send(err);

        if (req.body.email == null) {
        } else {
            user.email = req.body.email;
        }

        if (req.body.username == null) {
        } else {
            user.userName = req.body.username;
        }

        if (req.body.balance == null) {
        } else {
            console.log(req.body.balance);
            user.balance = parseInt(user.balance) + parseInt(req.body.balance);
        }

        if (req.body.phonenumber == null) {
        } else {
            user.phoneNumber = req.body.phonenumber;
        }

        if (req.body.isverified == null) {
        } else {
            user.isVerified = req.body.isverified;
        }

        user.save(function (err) {

            if (req.body.photo == null) {
                if (err) console.log(err);
                user.save(function (err) {
                    if (err) throw err;
                    //res.json({message: "User updated"});
                })
            } else {
                console.log('hi!');
                var uploadPhoto = function (photo, callback) {
                    console.log('im here');
                    cloudinary.uploader.upload(photo, function (result) {
                        user.photo = result.url;
                        console.log(result.url);
                        console.log(req.body.photo);
                        var callback = function () {
                            console.log('yeah');
                            if (err) console.log(err);
                            user.save(function (err) {
                                if (err) throw err;
                                //res.json({message: "User updated"});
                            })
                        }
                        callback();
                    })
                };
                uploadPhoto(req.body.photo);

                var callback = function () {
                        console.log('yeah');
                    if (err) console.log(err);
                    user.save(function (err) {
                        if (err) throw err;
                        //res.json({message: "User updated"});
                    })
                }
            };
            if (err) return res.send(err);
            res.json({message: "User updated"});

        });
    });
};

exports.deleteUser = function Delete(req, res) {
    User.remove({
        _id: req.user._id
    }, function (err) {

        if (err) return res.send(err);

        res.json({message: 'Successfully deleted'});
    });
};
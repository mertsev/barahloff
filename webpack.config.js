/**
 * Created by Vladand on 16.06.2017.
 */
let HtmlWebpackPlugin = require('html-webpack-plugin');
let path = require('path');
let webpack = require('webpack');
let bootstrapEntryPoint = require('./webpack.bootstrap.config');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

let isProd = process.env.NODE_ENV === 'production';

let cssDev = ['style-loader', 'css-loader', 'sass-loader'];
let cssProd = ExtractTextPlugin.extract(
    {
        publicPath: '../',
        fallback: 'style-loader',
        use: ['css-loader', 'sass-loader']
    }
);
let cssConfig = isProd ? cssProd : cssDev;
let bootstrapConfig = isProd ? bootstrapEntryPoint.prod : bootstrapEntryPoint.dev;

module.exports = {
    entry: {
        app: './front/src/index.js',
        bootstrap: bootstrapConfig
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        publicPath: '../',
        filename: '[name].bundle.js'
    },
    module: {
        rules: [
            {test: /\.scss$/, use: cssConfig},
            {test: /\.css$/, use: ['style-loader', 'css-loader']},
            {test: /\.js$/, exclude: /node_modules/, use: 'babel-loader'},
            {
                test: /\.(jpg|png|svg|gif)$/,
                use: [
                    "file-loader?name=images/[name].[ext]",
                    'image-webpack-loader'
                ]
            },
            {test: /\.(woff2?|svg)$/, loader: 'url-loader?limit=10000&name=fonts/[name].[ext]'},
            {test: /\.(ttf|eot|otf)$/, loader: 'file-loader?name=fonts/[name].[ext]'},
            {test: /bootstrap-sass[\/\\]assets[\/\\]javascripts[\/\\]/, loader: 'imports-loader?jQuery=jquery'}
        ]
    },
    devServer: {
        contentBase: path.join(__dirname, 'build'),
        compress: true,
        hot: true,
        open: true,
        stats: 'errors-only'
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Barahloff',
            template: './front/src/index.html'
        }),
        new ExtractTextPlugin({
            filename: '[name].css',

            disable: !isProd
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin()
    ]
}
import React from 'react';
import {Button, Grid, Row} from "react-bootstrap";
import $ from 'jquery';
import {browserHistory} from 'react-router';
import axios from 'axios';

export class UserInfo extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = ({
            userName: this.props.user.userName,
            email: this.props.user.email,
            phoneNumber: this.props.user.phonenumber,
            vk: this.props.user.vkId,
            userPhotoSrc: this.props.user.photo,
            userPhoto: this.props.user.photo,
            userPassword: '',
            newPassword:'',
            confirmPassword: '',
            editPassword: false,
            isChanged: true,
            isChangedUserPhoto: false,
            emailError: '',
            userNameError: '',
            phoneNumberError: '',
            userPasswordError: '',
            newPasswordError: '',
            confirmPasswordError: '',
            vkError: ''
        });

        this.changeProfile = this.changeProfile.bind(this);
        this.saveChangeProfile = this.saveChangeProfile.bind(this);
        this.inputChange = this.inputChange.bind(this);
        this.enterPassword = this.enterPassword.bind(this);
        this.loadImg = this.loadImg.bind(this);
    }

    changeProfile(){
        this.setState({
            editModeOn: !this.state.editModeOn,
            userName: this.props.user.userName,
            email: this.props.user.email,
            phoneNumber: this.props.user.phonenumber,
            userPhoto: this.props.user.photo
        })
        $('input').each(function() {
            let formGroup = $(this).parents('.form-group');
            let glyphicon = formGroup.find('.form-control-feedback');
            formGroup.removeClass('has-success');
            glyphicon.removeClass('glyphicon-ok');
        })
    }

    inputChange(event){
        let name = event.target.name;
        let value = event.target.value;
        this.setState({
            [name]: value,
            [`${name}Error`]: '',
            isChanged: false
        });
    }

    enterPassword(event){
        if(event.target.value===this.state.userPassword)
        {
            this.setState({
                editPassword: true
            })
        }
    }

    saveChangeProfile(){
        console.log("KNopochka nazhata");
        var self = this;
        var formValid = true;
        var FS = document.getElementById("confirmPassword");
        if (self.state.confirmPassword!==self.state.newPassword){
            FS.setCustomValidity("notConfirmPassword");
        }
        $('input').each(function() {
            var formGroup = $(this).parents('.form-group');
            var glyphicon = formGroup.find('.form-control-feedback');
            if (this.checkValidity()) {
                formGroup.addClass('has-success').removeClass('has-error');
                glyphicon.addClass('glyphicon-ok').removeClass('glyphicon-remove');
            } else {
                formGroup.addClass('has-error').removeClass('has-success');
                glyphicon.addClass('glyphicon-remove').removeClass('glyphicon-ok');
                formValid = false;
                self.setState({ isChanged: true });
                let msg = this.validationMessage;
                let err = this.name;
                self.setState({
                    [`${err}Error`]: msg
                })
            }
        })
        if (formValid) {
            console.log("forma valid");
            axios.put("/api/user/", {
                    photo:  self.state.userPhoto,
                    email: self.state.email,
                    username: self.state.userName,
                    phonenumber: self.state.phoneNumber,
                    vkid: self.state.vk
                })
                .then(function(response) {
                    alert("Данные успешно изменены");
                    document.getElementById('userImg').src = self.state.userPhoto;
                    self.setState({
                        editModeOn: false,
                        isChanged: true,
                        isChangedUserPhoto: false
                    });
                    let config={
                        you: true,
                        email: self.state.email,
                        userName: self.state.userName,
                        phoneNumber: self.state.phoneNumber,
                        vk: self.state.vk,
                        photo: self.state.userPhoto
                    };
                    self.props.update(config);
                    $('input').each(function() {
                        var formGroup = $(this).parents('.form-group');
                        var glyphicon = formGroup.find('.form-control-feedback');
                        formGroup.removeClass('has-success');
                        glyphicon.removeClass('glyphicon-ok');
                    })
                })
                .catch(function (error) {
                    console.log(error);
                    self.setState({
                        emailError: "Email уже занят.",
                        isChanged: true
                    });
                    $(document.getElementById('emailFormGroup')).addClass('has-error').removeClass('has-success');
                    $(document.getElementById('emailFormGroup')).find('.form-control-feedback')
                        .addClass('glyphicon-remove').removeClass('glyphicon-ok');
                });
        }
    }

    loadImg(event){
        var self = this;
        var fileInput = event.target;
        var file = fileInput.files[0];
        var fileReader = new FileReader();
        var d;
        var type=file.type;
        fileReader.readAsDataURL(file);
        fileReader.onload = function(event){
            d = event.target.result;
            d = d.replace("data:;","data:"+type+";");
            self.setState({
                userPhoto: d,
                isChanged: false,
                isChangedUserPhoto: true
            })
        }
        console.log(this.state.userPhoto);
        console.log("Izmenilas photockaaaaaaaaaaa");
        document.getElementById('userImg').src = window.URL.createObjectURL(file);
    }

    render() {
        return (
            <div>
                <div className="profile__userInfo">
                    <h1>{this.props.user.you ? "Мой профиль" : `Профиль пользователя ${this.props.user.userName}`}</h1>
                </div>
                <div className="profile__userInfo">
                    <div className="profile__img">
                        <div className="profile__img">
                            <img className="userImg" src={this.props.user.photo ? this.props.user.photo : require('../imgs/nophoto.gif')} id="userImg"></img>
                        </div>
                        <div className={this.state.editModeOn ? "change-photo" : "dissapearing"}>
                            <Button className="change-photo-button">Выбрать фото
                            <input encType="multipart/form-data" className={this.state.editModeOn ? "input-img": 'dissapearing'}
                                   type="file" accept="image\*" id="loadImg" onChange={this.loadImg}></input>
                            </Button>
                        </div>
                    </div>
                <div className="profile__userInfo">
                <div className="userInfoCol">
                    <form className="form-horizontal" encType="multipart/form-data">
                        <div className="form-group has-feedback">
                            <div className="input-group">
                                <label className="no-edit-label-fix" htmlFor="name">Имя: </label>
                                <label className={this.state.editModeOn ? "dissapearing" : "no-edit-label-fix"}>{this.props.user.userName}</label>
                                <input type="name" className={this.state.editModeOn ? "form-control" : "dissapearing"}
                                       required="required" id="userName"
                                       name="userName" onChange={this.inputChange}
                                       value={this.state.userName}/>
                                <span className="glyphicon form-control-feedback"></span>
                            </div>

                            <p className="help-block with-errors">{this.state.userNameError}</p>
                        </div>

                        <div className="form-group has-feedback">
                            <div className="input-group">
                                <label className="no-edit-label-fix" htmlFor="email">Электронная почта: </label>
                                <label className={this.state.editModeOn ? "dissapearing" : "no-edit-label-fix"}>{this.props.user.email}</label>
                                <input type="email" className={this.state.editModeOn ? "form-control" : "dissapearing"} required="required" id="email"
                                       name="email" onChange={this.inputChange} value={this.state.email}/>
                                <span className="glyphicon form-control-feedback"></span>
                            </div>
                            <p className="help-block with-errors">{this.state.emailError}</p>
                        </div>

                        <div className="form-group has-feedback">
                            <div className="input-group">
                                <label className="no-edit-label-fix" htmlFor="phoneNumber">Телефон:</label>
                                <label className={this.state.editModeOn ? "dissapearing" : "no-edit-label-fix"}>{this.props.user.phoneNumber}</label>
                                <input type="text" className={this.state.editModeOn ? "form-control" : "dissapearing"} id="phoneNumber"
                                       name="phoneNumber" onChange={this.inputChange} placeholder="формат ввода: 7(888)888-88-88"
                                       value={this.state.phoneNumber} pattern="7\([0-9]{3}\)[0-9]{3}-[0-9]{2}-[0-9]{2}"/>
                                <span className="glyphicon form-control-feedback"></span>
                            </div>
                            <p className="help-block with-errors">{this.state.phoneNumberError}</p>
                        </div>

                        <div className="form-group has-feedback">
                            <div className="input-group">
                                <label className="no-edit-label-fix">Страница VK: </label>
                                <label className={this.state.editModeOn ? "dissapearing" : "no-edit-label-fix"}>{this.props.user.vk}</label>
                                <input type="text" className={this.state.editModeOn ? "form-control" : "dissapearing"} id="vk"
                                       name="vk" onChange={this.inputChange} value={this.state.vk}/>
                                <span className="glyphicon form-control-feedback"></span>
                            </div>
                            <p className="help-block with-errors">{this.state.vkError}</p>
                        </div>
                    </form>
                </div>
                        <div className="dissapearing">
                        <form className={this.state.editModeOn ? "form-horizontal" : "dissapearing"} encType="multipart/form-data">
                            <p className="no-edit-label-fix">Изменение пароля</p>
                            <div className="form-group has-feedback">
                                <div className="input-group">
                                    <label className="no-edit-label-fix" htmlFor="userPassword">Старый пароль: </label>
                                    <label className={this.state.editModeOn ? "dissapearing" : "no-edit-label-fix"}></label>
                                    <input type="password" pattern="[^\s]{6,50}" className={this.state.editModeOn ? "form-control" : "dissapearing"}
                                           id="userPassword" name="userPassword" onChange={this.inputChange}
                                           value={this.state.userPassword}/>
                                    <span className="glyphicon form-control-feedback"></span>
                                </div>
                                <p className="help-block with-errors">{this.state.userPasswordError}</p>
                            </div>

                            <div className="form-group has-feedback">
                                <div className="input-group">
                                    <label className="no-edit-label-fix" htmlFor="newPassword">Новый пароль: </label>
                                    <label className={this.state.editModeOn ? "dissapearing" : "no-edit-label-fix"}></label>
                                    <input type="password" pattern="[^\s]{6,50}" className={this.state.editModeOn ? "form-control" : "dissapearing"} id="newPassword"
                                           name="newPassword" onChange={this.inputChange} value={this.state.newPassword}
                                           disabled={!this.state.editPassword}/>
                                    <span className="glyphicon form-control-feedback"></span>
                                </div>
                                <p className="help-block with-errors">{this.state.newPasswordError}</p>
                            </div>

                            <div className="form-group has-feedback">
                                <div className="input-group">
                                    <label className="no-edit-label-fix" htmlFor="confirmPassword">Повторите пароль:</label>
                                    <label className={this.state.editModeOn ? "dissapearing" : "no-edit-label-fix"}></label>
                                    <input type="url" className="form-control" id="confirmPassword"
                                           name="confirmPassword" onChange={this.inputChange}
                                           value={this.state.confirmPassword} disabled={!this.state.editPassword}/>
                                    <span className="glyphicon form-control-feedback"></span>
                                </div>
                                <p className="help-block with-errors">{this.state.confirmPasswordError}</p>
                            </div>
                        </form>
                </div>
                    </div>
                    <div className="profile-right-btn">
                    <Button className={this.props.user.you ? "change-profile-button" : "dissapearing"} onClick={this.changeProfile}>
                        <span className="glyphicon glyphicon-cog"/>
                    </Button>
                    <Button className={this.state.editModeOn ? "save-change-profile-button" : "dissapearing"}
                            onClick={this.saveChangeProfile}  name="saveChangesBtn" disabled={this.state.isChanged}>
                        <span className="glyphicon glyphicon-ok"/>
                    </Button>
                </div>
            </div>
            </div>
        )
    }
}
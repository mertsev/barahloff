import React from 'react';
import {Link} from "react-router";
import {Button, Col, Grid, Row} from "react-bootstrap";


export class HistoryFilter extends React.Component {
    render() {
        return(
            <div className="panel text">
                <Grid className="grid">
                    <Row className="show-grid">
                        <Col sm={6} md={3}>
                            <p>Поиск</p>
                            <input type="text" className="form-control"/>
                        </Col>
                        <Col sm={6} md={3}>
                            <p>Выберите категорию:</p>
                            <select className="form-control" id="category">
                                <option>Спорт</option>
                                <option>Раритет</option>
                                <option>Техника</option>
                                <option>Лакшери</option>
                            </select>
                        </Col>
                        <Col sm={6} md={3}>
                            <p><input type="checkbox"/>Участия в розыгрышах</p>
                            <p><input type="checkbox"/>Мои лоты</p>
                            <p><input type="checkbox"/>Только выигранные розыгрыши</p>
                        </Col>
                        <Col sm={6} md={3}>
                            <Button>Применить фильтр</Button>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
import React from 'react';
import {LogIn} from './LogIn';
import {SignUp} from './SignUp';
import {Link} from "react-router";
import {browserHistory} from 'react-router'
import $ from 'jquery'

export class Landing extends React.Component {
    constructor(props){
        super(props)

    }

    componentDidMount(){
        $('.question').click(function () {

            if ($($(this).attr('data-target')).attr("class") === 'collapse' )
            {
                $('.collapse.in').each(function () {
                    let openArea = $(this).attr('id');
                    $('.question').each(function () {
                        if($(this).attr('data-target') === '#' + openArea)
                            $(this).trigger('click');
                    })
                })
                $(this).children('.arrow').css( "transform" , "rotate(-180deg)" );
            }
            else if($($(this).attr('data-target')).attr("class") === 'collapse in' )
            {
                $(this).children('.arrow').css("transform","none" ) ;
            }
        })
    }
    render() {

        return (
            <div className="landing">
                <div className="landing__greeting" id="greeting">
                    <div className="greeting__block">
                        <div className="greeting__text">
                            <h1><b>Barahloff.ru</b> - испытай свою удачу</h1>
                            <h3>
                                Есть желание избавиться от ненужного барахла? <br/>
                                Выиграть IPhone за 10 рублей?
                            </h3>
                            <h2><b>Присоединяйся к нам прямо сейчас!</b></h2>
                            <div className="greeting__btns">
                                <LogIn checkAuthUser={this.props.checkAuthUser}/>
                                <SignUp checkAuthUser={this.props.checkAuthUser}/>
                            </div>
                        </div>
                        <div className="greeting__img">
                            <img src={require('../imgs/greetingImg.png')}/>
                        </div>
                    </div>
                </div>
                <div className="landing__opportunities" id="opportunities">
                    <h1>Участвуй или разыгрывай!</h1>
                    <div className="opportunities__block">
                        <div className="join">
                            <h2>Прими участие в розыгрыше!</h2>
                            <p>1. Зарегистрируйся на сайте</p>
                            <p>2. Выбери понравившийся лот</p>
                            <p>3. Сделай ставку и дождись окончания розыгрыша</p>
                            <p>4. Забери приз!</p>
                        </div>
                        <div className="create">
                            <h2>Создай свой лот!</h2>
                            <p>1. Зарегистрируйся на сайте</p>
                            <p>2. Нажми кнопку "Создать лот"</p>
                            <p>3. Заполни всю необходимую информацию о лоте</p>
                            <p>4. Дождись окончания розыгрыша и забери деньги!</p>
                        </div>
                    </div>
                </div>
                <div className="landing__specials" id="specials">
                    <h1>Особенности сервиса</h1>
                    <div className="specials__block">
                        <img id="img1" src={require('../imgs/info1.png')} alt=""/>
                        <img id="img2" src={require('../imgs/info2.png')} alt=""/>
                        <img id="img3" src={require('../imgs/info3.png')} alt=""/>
                        <img id="img4" src={require('../imgs/info4.png')} alt=""/>
                        <img id="img5" src={require('../imgs/info5.png')} alt=""/>
                        <div className="info" id="info1">
                            <h2>Простая навигация</h2>
                            <p>Вы можете легко попасть на любую страницу сайта</p>
                        </div>
                        <div className="info" id="info2">
                            <h2>Удобство</h2>
                            <p>Всегда можно посмотреть таймер и набранную сумму</p>
                        </div>
                        <div className="info" id="info3">
                            <h2>Удобный поиск</h2>
                            <p>Примените необходимые вам фильтры</p>
                        </div>
                        <div className="info" id="info4">
                            <h2>Испытай удачу!</h2>
                            <p>Нажимай "Участвовать" и получи шанс выиграть ценную вещь</p>
                        </div>
                        <div className="info" id="info5">
                            <h2>Разыграй вещь!</h2>
                            <p>Создай свой лот, нажав на эту кнопку</p>
                        </div>
                    </div>
                </div>
                <div className="landing__faq" id="faq">
                    <h1>Часто задаваемые вопросы</h1>
                    <div className="faq__block">
                        <button className="btn question"
                                type="button"
                                data-toggle="collapse"
                                data-target="#balance"
                                aria-expanded="false"
                                aria-controls="collapseExample">
                            <h2>- Как пополнить баланс?</h2>
                            <h2 className="arrow"><span className="glyphicon glyphicon-menu-down"></span></h2>
                        </button>
                        <div className="collapse" id="balance">
                            <p>Для пополнения баланса выполните следующие шаги:</p>
                            <p>1. Нажмите кнопку "Баланс" на шапке сайта</p>
                            <p>2. Введите данные карты и сумму пополнения</p>
                            <p>3. Перейдите на сайт платежной системы и переведите деньги на свой счет на сайте</p>
                            <p>4. Участвуйте в розыгрышах!</p>
                        </div>
                        <button className="btn question"
                                type="button"
                                data-toggle="collapse"
                                data-target="#favorite"
                                aria-expanded="false"
                                aria-controls="collapseExample">
                            <h2>- Что обозначает звездочка в углу лота?</h2>
                            <h2 className="arrow"><span className="glyphicon glyphicon-menu-down"></span></h2>
                        </button>
                        <div className="collapse" id="favorite">
                            <p>
                                Данный символ обозначает добавление лота в "Избранное". После нажатия
                                кнопка будет оранжевого цвета, и лот будет легко заметен при просмотре списка.
                                На сайте существует раздел "Избранное", перейти в него вы можете, нажав на символ
                                звезды в шапке сайта. В нем вы найдете все лоты, которые вы добавили в избранное
                                путем нажатя на звездочку в нижнем правом углу лота.
                            </p>
                        </div>
                        <button className="btn question"
                                type="button"
                                data-toggle="collapse"
                                data-target="#rules"
                                aria-expanded="false"
                                aria-controls="collapseExample">
                            <h2>- По каким правилам ведется розыгрыщ?</h2>
                            <h2 className="arrow"><span className="glyphicon glyphicon-menu-down"></span></h2>
                        </button>
                        <div className="collapse" id="rules">
                            <p>1. Пользователь создает лот, назначает минимальную сумму и дату розыгрыша</p>
                            <p>2. Если наступает момент розыгрыша, а минимальная сумма не набралась - деньги
                                возвращаются участникам, а лот автоматически удаляется</p>
                            <p>3. Если минимальная сумма набрана, то лот точно будет разыгран. При наступлении
                                даты розыгрыша, выбирается случайный победитель</p>
                            <p>4. 1 ставка - 10 рублей. Чем выше ставка, тем больше шанс на победу. Шанс
                                рассчитывается как Ш = С / (ОС + С) * 100%, где Ш - шанс победы, С - ваша суммарная ставка
                                ОС - общая набранная сумма</p>
                            <p>5. Победитель розыгрыша связывается с создателем лота, и когда первый подтвердит
                                получение приза, деньги перечислятся на счет создателя лота. В течение доставки
                                деньги находятся на сайте и недоступны для использования</p>
                        </div>
                        <button className="btn question"
                                type="button"
                                data-toggle="collapse"
                                data-target="#win"
                                aria-expanded="false"
                                aria-controls="collapseExample">
                            <h2>- Я выиграл приз. Что делать?</h2>
                            <h2 className="arrow"><span className="glyphicon glyphicon-menu-down"></span></h2>
                        </button>
                        <div className="collapse" id="win">
                            <p>Если вы победили в лоте, вы будете уведомлены об этом красным кружком в верхней
                                части сайта, рядом с вашим ником.</p>
                            <p>1. Перейдите в свой профиль</p>
                            <p>2. Перейдите на страницу лота, в которым вы победили</p>
                            <p>3. Нажмите на ник создателя лота и перейдите в его профиль</p>
                            <p>4. Найдите контакты создателя лота, свяжитесь с ним и договоритесь об отправке приза</p>
                            <p>5. После успешной доставки приза подтвердите получение посылки на сайте, и тогда
                                создатель лота получит деньги</p>
                        </div>
                    </div>
                </div>
                <div className="landing__team" id="team">
                    <h1>Команда разработчиков</h1>
                    <div className="team__block">
                        <div className="member">
                            <img src={require('../imgs/team1.png')} alt=""/>
                            <h2>Пушкарев Александр</h2>
                            <p>Менеджер</p>
                        </div>
                        <div className="member">
                            <img src={require('../imgs/team2.png')} alt=""/>
                            <h2>Соколовский Никита</h2>
                            <p>UX/UI дизайнер</p>
                        </div>
                        <div className="member">
                            <img src={require('../imgs/team3.png')} alt=""/>
                            <h2>Мерцев Сергей</h2>
                            <p>Back-end разработчик</p>
                        </div>
                        <div className="member">
                            <img src={require('../imgs/team4.png')} alt=""/>
                            <h2>Рыкова Анна</h2>
                            <p>Front-end разработчик</p>
                        </div>
                        <div className="member">
                            <img src={require('../imgs/team5.png')} alt=""/>
                            <h2>Андони Владислав</h2>
                            <p>Front-end разработчик</p>
                        </div>
                    </div>
                </div>
                <div className="grayWrapper">
                    <div className="landing__registration" id="registration">
                        <h1>Присоединяйся к нам!</h1>
                        <div className="registration__btns">
                            <LogIn checkAuthUser={this.props.checkAuthUser}/>
                            <SignUp checkAuthUser={this.props.checkAuthUser}/>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}
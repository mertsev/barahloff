import React from 'react';
import {Item} from './Item';
import {Link} from "react-router";

export class Favorites extends React.Component {
    render() {
        return (
            <div>
                <p>I am Favorites</p>
                <p><Link to="main">Main</Link></p>
                <p><Link to="profile">Profile</Link></p>
                <p><Link to="addMoney">AddMoney</Link></p>
                <p><Link to="favorites">Favorites</Link></p>
                <p><Link to="history">History</Link></p>
                <p><Link to="/">Exit (to Landing)</Link></p>
            </div>
        )
    }
}
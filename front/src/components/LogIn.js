import React from 'react';
import {Link, browserHistory} from "react-router";
import {Button, FormControl, Modal, OverlayTrigger, Popover, Tooltip} from "react-bootstrap";
import axios from 'axios';
import $ from 'jquery';

export class LogIn extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            password: '',
            confirmPassword:'',
            isConfirmedPassword: false,
            email: '',
            emailError: '',
            passwordError: '',
            confirmPasswordError: ''
        };
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.signup = this.signup.bind(this);
        this.enterPassword = this.enterPassword.bind(this);
        this.confirmPassword = this.confirmPassword.bind(this);
        this.enterEmail = this.enterEmail.bind(this);
    }

    open() {
        this.setState({
            showModal: true
        });
    }

    signup() {
        var self = this;
        var formValid = true;
        var CP = document.getElementById("confirmPassword");
        if (!this.state.isConfirmedPassword){
            CP.setCustomValidity("Подтверждение не совпадает с паролем.");
            // self.setState({confirmPasswordError: ''})
        }
        $('input').each(function() {
            var formGroup = $(this).parents('.form-group');
            var glyphicon = formGroup.find('.form-control-feedback');
            if (this.checkValidity()) {
                formGroup.addClass('has-success').removeClass('has-error');
                glyphicon.addClass('glyphicon-ok').removeClass('glyphicon-remove');
            } else {
                formGroup.addClass('has-error').removeClass('has-success');
                glyphicon.addClass('glyphicon-remove').removeClass('glyphicon-ok');
                let msg = this.validationMessage;
                let err = this.name;
                //console.log(err);
                self.setState({
                    [`${err}Error`]: msg
                })
                formValid = false;
            }
        })
        if (formValid) {
            axios.post('/api/register', {
                email: self.state.email,
                password: self.state.password
            })
                .then(function (response) {
                    self.close();
                    self.props.checkAuthUser(true);
                    browserHistory.push('/main');
                })
                .catch(function (error) {
                    self.setState({
                        emailError: "Email уже занят."
                    });
                    $(document.getElementById('emailFormGroup')).addClass('has-error').removeClass('has-success');
                    $(document.getElementById('emailFormGroup')).find('.form-control-feedback')
                        .addClass('glyphicon-remove').removeClass('glyphicon-ok');
                    // formValid = false;
                    console.log(error);
                });
        }
    }

    enterEmail(event){
        let str = event.target.value;
        this.setState({
            email: str,
            emailError: ''
        })
    }

    enterPassword(event) {
        let str = event.target.value;
        let checkPass = this.state.confirmPassword === str;
        this.setState({
            password: str,
            passwordError: '',
            isConfirmedPassword: checkPass
        })
    }

    confirmPassword(event) {
        event.target.setCustomValidity('');
        let str = event.target.value;
        let checkPass = this.state.password === str;
        this.setState({
            confirmPassword: str,
            isConfirmedPassword: checkPass,
            confirmPasswordError: ''
        });
    }

    close() {
        this.setState({showModal: false});
    }

    render() {
        const popover = (
            <Popover id="modal-popover" title="popover">
                very popover. such engagement
            </Popover>
        );
        const tooltip = (
            <Tooltip id="modal-tooltip">
                wow.
            </Tooltip>
        );
        return (
            <div>
                <Button bsSize="large" onClick={this.open}
                >
                    <h2 className="textBtnLndg">Регистрация</h2>
                </Button>

                <Modal bsSize="lg" show={this.state.showModal} onHide={this.close}>
                    <Modal.Header closeButton>
                        <Modal.Title>Регистрация</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <form role="form" className="form-horizontal" >
                            <div className="form-group has-feedback" id="emailFormGroup">
                                <label htmlFor="email" className="control-label col-xs-3">Email:</label>
                                <div className="col-xs-6">
                                    <div className="input-group">
                                        <span className="input-group-addon"><i className="glyphicon glyphicon-envelope"></i></span>
                                        <input type="email" className="form-control" required="required" name="email"
                                               onChange={this.enterEmail}/>
                                    </div>
                                    <span className="glyphicon form-control-feedback"></span>
                                    <p className="help-block with-errors">{this.state.emailError}</p>
                                </div>
                            </div>
                            <div className="form-group has-feedback">
                                <label htmlFor="password" className="control-label col-xs-3">Пароль:</label>
                                <div className="col-xs-6">
                                    <div className="input-group">
                                        <span className="input-group-addon"><i className="glyphicon glyphicon-lock"></i></span>
                                        <input type="password" className="form-control" required="required" name="password"
                                               pattern="[^\s]{6,50}" onChange={this.enterPassword}/>
                                    </div>
                                    <span className="glyphicon form-control-feedback"></span>
                                    <p className="help-block with-errors">{this.state.passwordError}</p>
                                </div>
                            </div>
                            <div className="form-group has-feedback">
                                <label htmlFor="password" className="control-label col-xs-3">Повтор пароля:</label>
                                <div className="col-xs-6">
                                    <div className="input-group">
                                        <span className="input-group-addon"><i className="glyphicon glyphicon-lock"></i></span>
                                        <input type="password" className="form-control" required="required" id="confirmPassword"
                                               pattern="[^\s]{6,50}" onChange={this.confirmPassword} name="confirmPassword"/>
                                    </div>
                                    <span className="glyphicon form-control-feedback"></span>
                                    <p className="help-block with-errors">{this.state.confirmPasswordError}</p>
                                </div>
                            </div>
                        </form>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button onClick={this.signup}>Зарегистрироваться</Button>
                        <Link to="main"><Button onClick={this.signup}>VK</Button></Link>
                    </Modal.Footer>
                </Modal>

            </div>
        );
    }
}
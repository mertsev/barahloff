import React from 'react';
import {Item} from './Item';
import {MainFilter} from './MainFilter';
import {Link} from "react-router";
import axios from 'axios';
import {Button, Col, Grid, Row, Table} from "react-bootstrap";


export class Main extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            showItems: []
        }
    }

    updateData(config){
        this.setState({showItems: config});
    }

    componentWillMount(){
        var self = this;
        axios.get('/api/lots/1')
            .then(function (response) {
                self.setState({showItems: response.data});
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        const showItems = this.state.showItems.map((item) => {
            return (<Item item={item}  key={item._id}/>);
        });

        return (
            <div className="Main">
                <div className="items">
                    <h1>Главная</h1>
                    <MainFilter items = {this.state.showItems} update={this.updateData.bind(this)}/>
                    <div className="itemList">
                        {showItems}
                    </div>
                    <div className="addButton">
                        <Link to="createLot" className="round red">+<span className="round">Создать лот</span></Link>
                    </div>
                </div>
            </div>
        )
    }
}
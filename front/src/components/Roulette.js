import React from 'react';
import {Link} from "react-router";
import $ from 'jquery'
import axios from 'axios'

export class Roulette extends React.Component {
    constructor(props){
        super(props);

        this.fillingRoulett = this.fillingRoulett.bind(this);

        this.state = {
            winner: '',
            rouletteArr: [],
            winnerIndex: '',
            winnerId: 0
        }
    }
    componentWillMount(){
        let self = this;
        let imageArray;
        let winner;
        let rouletteArray;
        let winnerIndex;
        let winnerId;
        axios.get('/api/betters/' + this.props.lotId)
            .then((res) => {
                imageArray = res.data.map(item =>{
                    console.log(item)
                    return item.photo ?  {img: item.photo, userId: item.id, userName: item.user} : {img: 'https://cinema.raindub.ru/wp-content/uploads/2017/01/user-icon.png', userId: item.id, userName: item.user}
                })

            }).then(() => {
            console.log(imageArray);
            axios.get('/api/lot/' + this.props.lotId)
                .then((res) =>{
                    winner = res.data._winner
                    console.log(winner);
                }).then(() => {
                    let lengthOfImgs, currentImg
                    rouletteArray = Array.apply(null, new Array(99)).map(function() { return 0; }).map((item, index) => {
                        lengthOfImgs = imageArray.length - 1;
                        currentImg = this.randomIneger(0, lengthOfImgs);
                        console.log(currentImg)
                        if(index > 40 && index < 60 && imageArray[currentImg].userId === winner){
                            winner = imageArray[currentImg].userName;
                            winnerIndex = index;
                            winnerId = imageArray[currentImg].userId
                        }
                        return(<img key={index} src={imageArray[currentImg].img} userId={imageArray[currentImg].userId}/>)
                    });
            }).then(()=>{
                self.setState({
                    winner: winner,
                    rouletteArr: rouletteArray,
                    winnerIndex: winnerIndex,
                    winnerId: winnerId
                })
            })

        });

    }
    fillingRoulett(){
        let self = this;
        console.log(this.state.winnerIndex);
        var option = {
            speed : 6,
            duration : 1,
            stopImageNumber : this.state.winnerIndex,
            startCallback : function() {
                console.log('start');
            },
            slowDownCallback : function() {
                console.log('slowDown');
            },
            stopCallback : function($stopElm) {
                self.props.wasDrawn(true,self.state.winner, self.state.winnerId);
            }
        };
        $('div.roulette').roulette(option);
    }
    roll(){
        $('div.roulette').roulette('start');
    }
    randomIneger(min, max){
        let rand = min + Math.random() * (max + 1 - min);
        rand = Math.floor(rand);
        return rand;
    }
    componentDidMount(){
        setTimeout(()=>this.fillingRoulett(),3000);
        setTimeout(() => {this.roll()
        },8000);
    }
    render() {
        return (
            <div>
                <div className="itemDetails__innerDrawWrapper">
                    <div className="itemDetails__rouletteWrapper">
                        <div className="roulette">
                            {this.state.rouletteArr}
                        </div>
                    </div>
                    <div className="arrow-up"></div>
                </div>
            </div>
        )
    }
}
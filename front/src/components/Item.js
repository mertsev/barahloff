import React from 'react';
import {Link} from "react-router";
import {Button, ButtonGroup, Image, Panel, Thumbnail} from "react-bootstrap";
import {Timer} from "./Timer";

export class Item extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isHot: false,
            isFav: false,
            itemDetails: '/itemDetails/' + this.props.item._id
        }
    }

    componentWillMount(){
        this.setState({
            isHot: (new Date(this.props.item.date).getTime()-Date.now()<86400000)
        })
    }

    addToFav()
    {
        this.setState({
            isFav: !this.state.isFav
        })
    }

    render() {
        var panelStyle = 'item';
        var starStyle = 'glyphicon glyphicon-star-empty';
        var sumStyle = '';
        if(this.props.item.currentContribution >= this.props.item.price) sumStyle = 'all-sum';
        if(this.state.isFav) starStyle = 'glyphicon glyphicon-star';
        return (
            <div className={panelStyle}>
                <div className={this.state.isHot ? 'hot' : 'hot dissapearing'}>
                    <img src={require('../imgs/hot.png')}/>
                </div>
                <div>
                    <Link to={this.state.itemDetails} className="link-style">
                        <div className="Item__img">
                            <img src={this.props.item.photo.length > 0 ? this.props.item.photo[0] : require('../imgs/nophoto.gif')}/>
                        </div>
                        <div className="item__title">
                            <h2><b>{this.props.item.title}</b></h2>
                        </div>

                    </Link>
                </div>
                <Timer comingDate={Date.parse(this.props.item.date)}/>
                <p>Собрано/минимальная сумма:</p>
                <div className="sum">
                    <h2><b className={sumStyle}>{this.props.item.currentContribution}</b><b>
                        /{this.props.item.price}</b></h2>
                </div>
                <Link to={this.state.itemDetails}>
                    <Button className="button mkbetBtn middle-text">Участвовать</Button>
                </Link>&nbsp;
                <Button className={this.state.isFav ? 'button btnFav' : 'button'} onClick={()=>this.addToFav()}><span className={starStyle}></span></Button>
            </div>
        )
    }
}

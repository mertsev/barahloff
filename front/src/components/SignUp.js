import React from 'react';
import {Link, browserHistory} from "react-router";
import {Button, ButtonToolbar, FormControl, Modal, Popover, Tooltip} from "react-bootstrap";
import axios from 'axios'
import $ from 'jquery'

export class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {showModal: false};
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.login = this.login.bind(this);

        this.state = {
            redirect: false,
            invalid: false
        }
    }

    login(){
        let formValid = true;
        let self = this;
        //перебрать все элементы управления input
        $('input').each(function() {
            //найти предков, которые имеют класс .form-group, для установления success/error
            let formGroup = $(this).parents('.form-group');
            //найти glyphicon, который предназначен для показа иконки успеха или ошибки
            let glyphicon = formGroup.find('.form-control-feedback');
            //для валидации данных используем HTML5 функцию checkValidity
            if (this.checkValidity()) {
                //добавить к formGroup класс .has-success, удалить has-error
                formGroup.removeClass('has-error');
                //добавить к glyphicon класс glyphicon-ok, удалить glyphicon-remove
                glyphicon.removeClass('glyphicon-remove');
            } else {
                //добавить к formGroup класс .has-error, удалить .has-success
                formGroup.addClass('has-error');
                //добавить к glyphicon класс glyphicon-remove, удалить glyphicon-ok
                glyphicon.addClass('glyphicon-remove');
                //отметить форму как невалидную
                formValid = false;
            }
        });
        //если форма валидна, то
        if (formValid) {

            //сркыть модальное окно
            //отобразить сообщение об успехе
            axios.post('/api/login', {
                email: document.getElementById('inputEmail').value,
                password: document.getElementById('inputPassword').value
            }).then((res) => {
               if(res.data !== null) {
                   self.setState({
                    redirect: true
                    });
                   self.close();
                   self.props.checkAuthUser(true);
               } else
                   self.setState({
                       invalid: true
                   })
            }).catch((err) => {
                console.log('err: ' + err)
            });
        }
    }
    open() {
        this.setState({showModal: true});
    }

    close() {
        this.setState({showModal: false});
    }

    render() {
        const popover = (
            <Popover id="modal-popover" title="popover">
                very popover. such engagement
            </Popover>
        );
        const tooltip = (
            <Tooltip id="modal-tooltip">
                wow.
            </Tooltip>
        );
        if(this.state.redirect) browserHistory.push('/main');
        return (
            <div>

                <Button
                    bsSize="large"
                    onClick={this.open}
                >
                    <h2 className="textBtnLndg">Вход</h2>
                </Button>

                <Modal id="loginModal" bsSize="small" show={this.state.showModal} onHide={this.close}>
                    <Modal.Header closeButton>
                        <Modal.Title>Вход</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <form  role="form" id="loginForm">
                            <div className="form-group has-feedback">
                                <h4>Введите почту:</h4>
                                <div className="input-group">
                                    <span className="input-group-addon"><i className="glyphicon glyphicon-envelope"></i></span>
                                    <input
                                        type="email"
                                        placeholder="Email"
                                        className="form-control"
                                        id="inputEmail"
                                        name="email"
                                        required="required"
                                    />
                                    <span className="glyphicon form-control-feedback"></span>
                                </div>

                            </div>
                            <div className="form-group has-feedback">
                                <h4>Введите пароль:</h4>
                                <div className="input-group">
                                    <span className="input-group-addon"><i className="glyphicon glyphicon-lock"></i></span>
                                    <input
                                        type="password"
                                        placeholder="Пароль"
                                        className="form-control"
                                        id="inputPassword"
                                        name="password"
                                        required="required"
                                        minLength="6"
                                    />
                                    <span className="glyphicon form-control-feedback"></span>
                                </div>
                            </div>
                            <div className={this.state.invalid ? "alert alert-danger" : "alert alert-danger dissapearing"}>
                                <p>Введен неверный логин и/или пароль</p>
                            </div>
                        </form>
                    </Modal.Body>

                    <Modal.Footer>
                        <ButtonToolbar>
                            <Button onClick={this.login}><b>Войти</b></Button>&nbsp;
                            <Link to="main"><Button bsStyle="primary" onClick={this.login}>VK</Button></Link>
                        </ButtonToolbar>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}
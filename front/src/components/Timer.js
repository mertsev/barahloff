import React from 'react';

export class Timer extends React.Component
{
    constructor(props){
        super(props);
        this.state = {
            date: new Date()
        };
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        if(this.props.comingDate - this.state.date < 1){
            if(this.props.drawTime)
                this.props.drawTime(true);
            this.setState({
                date: 0
            });
            clearInterval(this.timerID)
        }else {
            this.setState({
                date: Date.now()
            });
        }
    }

    render(){
        if(this.state.date !== 0)
            return(
                <div>
                    <h2>
                        <b>{Math.floor((this.props.comingDate - this.state.date)/(1000*60*60*24))}&nbsp;:&nbsp;</b>
                        <b>{Math.floor((this.props.comingDate - this.state.date)/(1000*60*60)%24)}&nbsp;:&nbsp;</b>
                        <b>{Math.floor((this.props.comingDate - this.state.date)/(1000*60)%60)}&nbsp;:&nbsp;</b>
                        <b>{Math.floor((this.props.comingDate - this.state.date)/(1000)%60)}</b>
                    </h2>
                </div>
            )
        else
            return(
                <div>
                    <h2>
                        <b>0&nbsp;:&nbsp;</b>
                        <b>0&nbsp;:&nbsp;</b>
                        <b>0&nbsp;:&nbsp;</b>
                        <b>0</b>
                    </h2>
                </div>
            )
    }
}
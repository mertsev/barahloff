import React from 'react';
import {UserInfo} from './UserInfo';
import {ProfileLots} from './ProfileLots';
import {ProfileBets} from './ProfileBets';
import {Link} from "react-router";
import {Accordion, Button, Panel} from "react-bootstrap";
import {Item} from "./Item";
import axios from 'axios';

export class Profile extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = {
            myWinsIds:[],
            myWins: [],
            myWinsHide: true,
            myBetsIds: [],
            myBets: [],
            myBetsHide: true,
            myLots: [],
            myLotsHide: true,
            userInfo: []
        }
        this.aClick = this.aClick.bind(this);
    }

    aClick(event){
        var name = event.target.id;
        console.log(name);
        this.setState({
            [name]: !this.state[name]
        })
    }

    componentDidMount(){
        axios.get('/api/user/' + this.props.params.id)
            .then((res) => {
                this.setState({
                    userInfo: res.data
                })
            }).then(()=>{
                axios.get('/api/userlots/'+this.props.params.id)
                    .then((res)=> {
                    this.setState({
                        myLots: res.data
                    })
                    });
                if(this.state.userInfo.you) {
                    axios.get('/api/userbets')
                        .then((res) => {
                            this.setState({
                                myBetsIds: res.data
                            })
                        }).then(()=> {
                            let myBetsIds = this.state.myBetsIds;
                            if(myBetsIds!==(undefined || null)){
                            for (var i = 0; i < myBetsIds.length; i++)
                            {
                                axios.get('/api/lot/'+myBetsIds[i])
                                    .then((res)=>{
                                        this.setState((prevState)=>{
                                            return {
                                                myBets: [...prevState.myBets, res.data]
                                            }
                                        })
                                    })
                            }
                            }
                        });

                    axios.get('/api/wonlots')
                        .then((res) => {
                            this.setState({
                                myWins: res.data
                            })
                        });
                }
        })
    }

    updateData(config){
        this.setState({
            userInfo: config
        })
    }

    render() {
        return (
            <div className="profile">
                <UserInfo user={this.state.userInfo} update={this.updateData.bind(this)}/>
                <div className="clearfix"></div>
                <div className={this.state.userInfo.you ? "profile__items" : "dissapearing"}>
                    <div className="panel-group">
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h4 className="panel-title">
                                    Мои выигрыши
                                    <Button data-toggle="collapse" href="#collapse1" name="myWinsHide"
                                            className="hide-button"
                                            onClick={this.aClick}>
                                        {this.state.myWinsHide ?
                                            <div id="myWinsHide" onClick={this.aClick}>
                                                Развернуть <span className="glyphicon glyphicon-menu-down"></span>
                                            </div>
                                            :
                                            <div id="myWinsHide" onClick={this.aClick}>
                                                Свернуть <span className="glyphicon glyphicon-menu-up"></span>
                                            </div>
                                        }
                                    </Button>
                                </h4>
                            </div>
                            <div id="collapse1" className="panel-collapse collapse">
                                <div className="panel-body">
                                            {
                                                this.state.myWins!==(undefined || null) ?
                                                    this.state.myWins.map((item)=>{
                                                        return(<Item item={item} key={item.id}/>);})
                                                    :
                                                    <label>У вас еще нет выигрышей</label>
                                            }
                                </div>
                            </div>
                        </div>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h4 className="panel-title">
                                    Мои ставки ({this.state.myBets!==undefined ? this.state.myBets.length : '0'})
                                    <Button data-toggle="collapse" href="#collapse2" id="myBetsHide"
                                            className="hide-button"
                                            onClick={this.aClick}>{this.state.myBetsHide ?
                                        <div id="myBetsHide" onClick={this.aClick}>
                                            Развернуть <span className="glyphicon glyphicon-menu-down"></span>
                                        </div>
                                        :
                                        <div id="myBetsHide" onClick={this.aClick}>
                                            Свернуть <span className="glyphicon glyphicon-menu-up"></span>
                                        </div>
                                            }
                                    </Button>
                                </h4>
                            </div>
                            <div id="collapse2" className="panel-collapse collapse">
                                <div className="panel-body">
                                            {
                                                this.state.myBets !== (undefined || null) ?
                                                this.state.myBets.map((item) => {
                                                    return (<Item item={item} key={item.id}/>);
                                                })
                                                    :
                                                    <label>У вас еще нет ставок</label>
                                            }

                                </div>
                            </div>
                        </div>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h4 className="panel-title">
                                    Мои лоты ({this.state.myLots!==undefined ? this.state.myLots.length : '0'})
                                    <Button data-toggle="collapse" href="#collapse3" name="myLotsHide"
                                            className="hide-button"
                                            onClick={this.aClick}>{this.state.myLotsHide ?
                                        <div id="myLotsHide" onClick={this.aClick}>
                                            Развернуть <span className="glyphicon glyphicon-menu-down"></span>
                                        </div>
                                        :
                                        <div id="myLotsHide" onClick={this.aClick}>
                                            Свернуть <span className="glyphicon glyphicon-menu-up"></span>
                                        </div>
                                            }
                                    </Button>
                                </h4>
                            </div>
                            <div id="collapse3" className="panel-collapse collapse">
                                <div className="panel-body">
                                            {
                                                this.state.myLots!==(undefined || null) ?
                                                    this.state.myLots.map((item)=>{
                                                        return(<Item item={item} key={item.id}/>);})
                                                    :
                                                    <label>У вас еще нет лотов</label>
                                            }

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div className={this.state.userInfo.you ? "dissapearing" : "profile__items"}>
                    <div className="panel-group">
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h4 className="panel-title">
                                    Лоты пользователя ({this.state.myLots!==undefined ? this.state.myLots.length : '0'})
                                    <Button data-toggle="collapse" href="#collapse4" name="myLotsHide"
                                            className="hide-button"
                                            onClick={this.aClick}>{this.state.myLotsHide ?
                                        <div id="myLotsHide" onClick={this.aClick}>
                                            Развернуть <span className="glyphicon glyphicon-menu-down"></span>
                                        </div>
                                        :
                                        <div id="myLotsHide" onClick={this.aClick}>
                                            Свернуть <span className="glyphicon glyphicon-menu-up"></span>
                                        </div>
                                    }</Button>
                                </h4>
                            </div>
                            <div id="collapse4" className="panel-collapse collapse">
                                <div className="panel-body">

                                        <div className="panel-body">
                                            {this.state.myLots !== (undefined || null) ?
                                                this.state.myLots.map((item) => {
                                                    return (<Item item={item} key={item.id}/>);
                                                })
                                                :
                                                'У пользователя еще нет лотов'
                                            }
                                        </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
import React from 'react';
import {Link} from "react-router";

export class NavBar extends React.Component {
    render() {
        return (
            <div>
                <p>I am NavBar</p>
                <p><Link to="main">Main</Link></p>
                <p><Link to="profile">Profile</Link></p>
                <p><Link to="addMoney">AddMoney</Link></p>
                <p><Link to="favorites">Favorites</Link></p>
                <p><Link to="history">History</Link></p>
                <p><Link to="/">Exit (to Landing)</Link></p>
            </div>
        )
    }
}
import React from 'react';
import {Link} from "react-router";
import {Button, FormControl, Modal, Popover, Tooltip} from "react-bootstrap";
import axios from 'axios';

export class AddMoney extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            balance: this.props.balance,
            addBtnDisabled: true
        };
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.addMoney = this.addMoney.bind(this);
        this.changeSum = this.changeSum.bind(this);
    }

    open() {
        this.setState({showModal: true});
    }

    close() {
        this.setState({showModal: false});
    }

    addMoney(){
        var self=this;
        var money = document.getElementById("sum").value;
        axios.put("/api/user/", {
            balance:  money
        })
            .then(function (response) {
                alert("Баланс успешно пополнен");
                self.setState({
                    showModal: false
                });
                self.props.checkAuthUser(true);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    changeSum(event){
        var sum = event.target.value;
        this.setState({addBtnDisabled: sum <= 0 || sum >= 999999999999999999999-(+this.props.balance)});
    }

    render() {
        return (
            <div>
                <Button bsSize="large" onClick={this.open} id="balanceBtn">
                    Баланс: {this.props.balance}р
                </Button>

                <Modal bsSize="small" show={this.state.showModal} onHide={this.close} className="addMoney">
                    <Modal.Header closeButton>
                        <Modal.Title>Пополнение баланса</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h4>Введите сумму:</h4>
                        <FormControl id="sum" type="number" className="input" min="1" max={999999999999999999999-(+this.props.balance)} onChange={this.changeSum}/>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.addMoney} disabled={this.state.addBtnDisabled}>Пополнить баланс</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}
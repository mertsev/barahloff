import React from 'react';
import {Button, Col, FormControl, Grid, Row} from "react-bootstrap";

export class MainFilter extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            searchString: '',
            category: '',
            isMin: false,
            isHot: false
        };
        this.updateSearchStr = this.updateSearchStr.bind(this);
        this.updateCategory = this.updateCategory.bind(this);
        this.onFilter = this.onFilter.bind(this);
        this.updateHot = this.updateHot.bind(this);
        this.updateMin = this.updateMin.bind(this);
    }

    updateSearchStr(event)
    {
        const searchString = event.target.value.toLowerCase();
        this.setState({searchString: searchString})
        let filter = this.props.items.filter(item => {
            return item.title.toLowerCase().includes(searchString)
        })
        this.props.update(filter);
    }

    updateCategory(event)
    {
        const searchCategory = event.target.value;
        this.setState({category: searchCategory})
        const filter = this.props.items.filter(item=>{
            return item.category == searchCategory
        })
        this.props.update(filter);
    }
    updateMin(event){
        this.setState({isMin: !this.state.isMin});
        const filter = this.props.items.filter(item => {
            return item.currentContribution>=item.price >= !this.state.isMin
        })
        this.props.update(filter);
    }
    updateHot(event){
        this.setState({isHot: !this.state.isHot});
        const filter = this.props.items.filter(item => {
            return item.date-Date.now()<86400000 >= !this.state.isHot
        })
        this.props.update(filter);
    }

    onFilter(event)
    {
    }

    render() {
        return (
            <div className="panel text">
                <div className="filter">
                    <h2><b>Поиск</b></h2>
                    <input type="text" className="form-control" placeholder="Поиск..." value={this.state.value}
                           onChange={this.updateSearchStr}/>
                </div>
                <div className="filter">
                    <h2>Выберите категорию:</h2>
                    <select className="form-control" id="category" value={this.state.category} onChange={this.updateCategory}>
                        <option>Спорт</option>
                        <option>Раритет</option>
                        <option>Техника</option>
                        <option>Лакшери</option>
                    </select>
                </div>
                <div className="filter">
                    <div className="checkbox">
                        <input type="checkbox" onChange={this.updateMin}/><p>Собрана минимальная сумма</p>
                        <input type="checkbox" onChange={this.updateHot}/><p>Меньше суток до розыгрыша</p>
                    </div>
                </div>
                <div className="filter">
                    <Button onClick={this.onFilter}><h2>Применить фильтр</h2></Button>
                </div>
            </div>
        )
    }
}
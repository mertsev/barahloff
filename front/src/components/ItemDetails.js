import React from 'react';
import {Button, ProgressBar} from 'react-bootstrap'
import {Roulette} from './Roulette';
import {Timer} from './Timer'
import {browserHistory} from "react-router";
import '../lib/roulette'
import 'jquery-ui'
import axios from 'axios'
import $ from 'jquery'

export class ItemDetails extends React.Component {
    constructor(props){
        super(props);

        this.getCategory = this.getCategory.bind(this);
        this.getCreator = this.getCreator.bind(this);
        this.addBet = this.addBet.bind(this);
        this.changeBet = this.changeBet.bind(this);
        this.drawTime = this.drawTime.bind(this);
        this.wasDrawn = this.wasDrawn.bind(this);

        this.state = {
            bet: 0,
            chance: 0,
            betBtnDisabled: true,
            isFavorite: this.props.route.isFavorite ? 'glyphicon glyphicon-star' : 'glyphicon glyphicon-star-empty',
            draw: false,
            wasDrawn: false,
            yourBet: 0,

            title: '',
            description: '',
            essentialSum: 0,
            timer: '',
            currentSum: 0,
            creator: '',
            creatorId: null,
            category: '',
            photo: [],
            winner: '',
            winnerId: 0,

            errorMessage: ''
        }
    }

    componentDidMount(){
        let self = this;
        let item;
        let yourBet;
        axios.get('/api/lot/' + this.props.params.id)
            .then((res) => {
                this.getCategory(res.data._category);
                this.getCreator(res.data._creator);
                item = res.data;
            }).then(()=>{
                axios.post('/api/betamount/', {
                    lotid: item._id
                }).then((res)=> {
                    yourBet = res.data;
                    self.setState({
                        title: item.title,
                        description: item.description,
                        essentialSum: item.price,
                        timer: item.date,
                        currentSum: item.currentContribution,
                        yourBet: yourBet,
                        draw: Date.parse(self.state.timer) === Date.now(),
                        wasDrawn: Date.parse(self.state.timer) < Date.now(),
                        photo: item.photo
                    })
                    }).then(()=>{
                        self.setState({
                            chance: Math.round((self.state.yourBet/self.state.currentSum)*100)
                        })
                    }).then(()=>{
                    $(function(){
                        let photos=self.state.photo;
                        console.log(photos.length);
                        if(photos.length > 0) {
                            $(document.getElementById('mainImg')).append("<img src=" + photos[0] + "></img>");
                            photos.shift();
                            $.each(photos, function (value) {
                                $(document.getElementById('secondaryImg')).append("<div class='secondary-img-div' id=" + value + "></div>");
                                $(document.getElementById(value)).append("<img src=" + photos[value] + "></img>");
                                console.log(value, photos[value]);
                            })
                        } else {
                            $(document.getElementById('mainImg')).append("<img src="+require('../imgs/nophoto.gif')+"></img>");
                        }

                    })
                    }
                )
                }
            );

    }

    getCategory(id){
        let self = this;
        axios.get('/api/category/' + id).then((res) => {
            self.setState({
                category: res.data.title
            })
        });
    }

    getCreator(id){
        let self = this;
        axios.get('/api/user/' + id).then((res) => {
            self.setState({
                creator: res.data,
                creatorId: id
            })
        });
    }

    changeBet(e){
        let bet = e.target.value;
        this.setState({
            bet: bet,
            chance: Math.round((+bet + +this.state.yourBet) / (+this.state.currentSum + +bet) * 100),
            betBtnDisabled: bet<10 || bet%10!==0,
            errorMessage: ''
        })
    }

    toFavorite(){
        this.setState({
            isFavorite: this.state.isFavorite === 'glyphicon glyphicon-star' ? 'glyphicon glyphicon-star-empty' : 'glyphicon glyphicon-star'
        })
    }

    backToMain(){
        browserHistory.push('/main');
    }
    drawTime(isDraw){
        if(isDraw)
            this.setState({
                draw: true
            });
    }
    addBet(){
        let self=this;
        axios.post('/api/lots', {
            amount: this.state.bet,
            lotid: this.props.params.id
            }).then(function (response) {
                self.setState({
                    currentSum: +self.state.currentSum + +self.state.bet,
                    yourBet: +self.state.yourBet + +self.state.bet
                });
                alert("Ставка сделана!");
            })
            .catch(function (error) {
                self.setState({
                    errorMessage: "Недостаточно средств."
                });
                console.log(error);
            });
    }
    renderRoulette(){
        if(this.state.draw) {
            return (<Roulette lotId={this.props.params.id} wasDrawn={this.wasDrawn}/>)
        }
    }
    wasDrawn(isEnd, winner,winnerId){
        if(isEnd)
            this.setState({
                wasDrawn: true,
                winner: winner,
                winnerId: winnerId
            });
    }
    render() {
        let {title, description, essentialSum, timer,currentSum, creator, category} = this.state;
        return (
            <div className="itemDetails">
                <div className="itemDetails__title">
                    <h2>{title}</h2>
                    <Button className={this.props.route.isFavorite ? "itemDetails__favoriteBtn btnFav" : "itemDetails__favoriteBtn"}
                            bsSize="large"
                            onClick={() => this.toFavorite()}>
                        <span className={this.state.isFavorite}></span>
                    </Button>
                </div>
                <p>Категория: {category}</p>
                <div className="itemDetails__info">
                    <div className="itemDetails__imgs" id="photos">
                        <div className="main__img" id="mainImg"></div>
                        <div className="secondary__img" id="secondaryImg"></div>
                    </div>
                    <div className="itemDetails__description">
                        <div className="itemDetails__descriptionField">{description}</div>
                        <div className="itemDetails__inline">
                            <ProgressBar className="itemDetails_progressBar" bsStyle="warning" now={(currentSum/essentialSum)*100}/>
                            <b className="itemDetails__bold">&nbsp;{currentSum}/{essentialSum}</b>
                        </div>
                        <div className="itemDetails__inline">
                            <p>Осталось времени:&nbsp;</p>
                            <div className="itemDetails__timer ">
                                <Timer comingDate={Date.parse(timer)} drawTime={this.drawTime}/>
                            </div>
                        </div>
                        <p className={this.state.creator.you ? "" : "dissapearing"}>Создатель лота:&nbsp;
                            <a href={"/profile/" + this.state.creatorId}>Вы</a></p>
                        <p className={this.state.creator.you ? "dissapearing" : ''}>Ваш взнос: <b className="itemDetails__bold">{this.state.yourBet}</b> рублей</p>
                        <p className = {this.state.creator.you ? "dissapearing" : ''}>Шанс выигрыша:  <b className="itemDetails__bold">{this.state.chance}</b>%</p>
                        <div className={this.state.wasDrawn || this.state.creator.you ? "dissapearing" : "itemDetails__makeBet"}>
                            <p className="itemDetails__small">Введите сумму, кратную 10.</p>
                            <form className="form-horizontal" id="betForm" action="/placeBet" method="post">
                                <div className="form-group">
                                    <input type="number" className="form-control itemDetails__input" id="bet" disabled={this.state.draw}
                                           name="bet" min="10" step="10" max="999999999999999999999"
                                           onChange={this.changeBet} onKeyDown={this.changeBet}/>
                                    <Button bsSize="small" className="itemDetails__betBtn" form="betForm" onClick={this.addBet}
                                            disabled={this.state.betBtnDisabled || this.state.draw}><p>Сделать ставку</p></Button>
                                    <b className="itemDetails__small error">{this.state.errorMessage}</b>
                                    <div>
                                        <Button className="itemDetails__backBtn" onClick={() => this.backToMain()} bsSize="small"><p>Назад</p></Button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className={this.state.wasDrawn ? "" : "dissapearing"}>
                            <h3>Победитель: <a href={"/profile/" + this.state.winnerId}>{this.state.winner}</a></h3>
                        </div>
                    </div>
                </div>
                <p className={this.state.creator.you ? "dissapearing" : ""}>Создатель лота:&nbsp;
                    <a href={"/profile/" + this.state.creatorId}>{creator.userName}</a></p>
                <div className={this.state.draw ? "itemDetails__draw" : "itemDetails__draw dissapearing"}>
                    {this.renderRoulette()}
                </div>
                <div className={this.state.creator.you && !this.state.draw ? "itemDetails__draw" : "itemDetails__draw dissapearing"}>
                    <div className="itemDetails__innerDrawWrapper">
                        <div className="itemDetails__rouletteWrapper">
                            <p className="itemDetails__bold">Рулетка запустится автоматически после окончания таймера</p>
                        </div>
                    </div>
                    <div className="arrow-up"></div>
                </div>
            </div>
        )
    }
}

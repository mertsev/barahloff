import React from 'react';
import {Header} from './Header';
import {Footer} from './Footer';
import {browserHistory} from 'react-router'
import axios from 'axios'

export class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            username: '',
            balance: 0,
            userIsAuth: false
        }
        this.checkAuthUser = this.checkAuthUser.bind(this);
    }

    componentWillMount() {
        let self = this;
        axios.get('/api/user').then((res) => {
            if (res.data.userName) {
                self.setState({
                    userId: res.data._id,
                    username: res.data.userName,
                    balance: res.data.balance,
                    userIsAuth: true
                });
                this.redirect()
            } else {
                self.setState({
                    userID: '',
                    username: '',
                    balance: 0,
                    userIsAuth: false
                });
                if(browserHistory.getCurrentLocation().pathname !== '/')
                    browserHistory.push('/')
            }
        }).catch((err) => {
            console.log(err)
        });
    }

    redirect() {
        if(browserHistory.getCurrentLocation().pathname === '/')
            browserHistory.push('/main');
    }

    render() {
        let children = React.Children.map(this.props.children, (child) => {
            return React.cloneElement(child, {
                checkAuthUser: this.checkAuthUser
            });
        });
        return (
            <div className="container-fluid">
                <Header userId={this.state.userIsAuth ? this.state.userId : 0} username={this.state.username} balance={this.state.balance} checkAuthUser={this.checkAuthUser}/>
                <div className="content">
                    {children}
                </div>
                <Footer/>
            </div>
        )
    }

    checkAuthUser(isAuth) {
        if (isAuth) {
            let self = this;
            axios.get('/api/user').then((res) => {
                if (typeof res.data.userName) {
                    self.setState({
                        username: res.data.userName,
                        balance: res.data.balance,
                        userIsAuth: true
                    });
                    this.redirect()
                }
            });
        } else {
            this.setState({
                userIsAuth: false,
                username: '',
                balance: 0,
            })
        }
    }
}
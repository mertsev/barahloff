import React from 'react';
import {Button} from 'react-bootstrap';
import {AddMoney} from "./AddMoney";
import {browserHistory, Link, Route, Router} from "react-router";
import {Favorites} from "./Favorites";
import axios from 'axios'
import $ from 'jquery'

export class Header extends React.Component {
    constructor(props){
        super(props)

        this.logout = this.logout.bind(this)
    }

    logout(){
        let self = this;
        axios.get('/api/logout').then(()=>{
            self.props.checkAuthUser(false);
            browserHistory.push('/');
        })
    }
    componentWillMount(){
        let anchor = ['#greeting','#opportunities','#specials','#faq','#team','#registration' ];
        $(document).on('click', 'a', function(event){
            let href =  $.attr(this,'href');
            if(anchor.indexOf(href) !== -1) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: $($.attr(this, 'href')).offset().top - 60
                }, 500);
            }
        });
    }
    render() {
        return (
            <div className="header" >
                <div className="header__content">
                    <div className="header__title">
                        <h1><b><a href="/main">Barahloff.ru</a></b></h1>
                    </div>
                    <div className={this.props.userId !== 0 ? "header__navbar" : "header__navbar dissapearing"}>
                        <AddMoney balance={this.props.balance} checkAuthUser={this.props.checkAuthUser}/>
                        <Link to="Favorites">
                            <button type="button" className="header__btn header__favoritesBtn btn btn-default btn-lg">
                                <span className="glyphicon glyphicon-star"></span>
                            </button>
                        </Link>
                        <div className="dropdown">
                            <button className="header__profileBtn header__btn btn-lg btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                {this.props.username}
                                <span className="glyphicon glyphicon-user"></span>
                                <span className="caret"></span>
                            </button>
                            <ul className="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation"><a role="menuitem" tabIndex="-1" href={"/profile/"+this.props.userId}>Профиль</a></li>
                                <li role="presentation"><Link role="menuitem" tabIndex="-1" to="history">История</Link></li>
                                <li role="presentation" className="divider" href="#"></li>
                                <li role="presentation"><a role="menuitem" onClick={this.logout} tabIndex="-1" href="/">Выйти</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className={this.props.userId === 0 ? "header__navLanding" : "header__navLanding dissapearing"}>
                        <a href="#greeting" ><h3>Приветствие</h3></a>
                        <a href="#opportunities" ><h3>Возможности</h3></a>
                        <a href="#specials" ><h3>Особенности</h3></a>
                        <a href="#faq" ><h3>FAQ</h3></a>
                        <a href="#team" ><h3>Команда</h3></a>
                        <a href="#registration" ><h3><b>Регистрация</b></h3></a>
                    </div>
                </div>
            </div>
        )
    }
}
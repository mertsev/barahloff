import React from 'react';
import {Button} from 'react-bootstrap'
import Datetime from 'react-datetime'
import {DropzoneComponent} from 'react-dropzone-component'
import axios from 'axios'
import {browserHistory} from 'react-router'
import $ from 'jquery'
//import {Link} from "react-router";

export class CreateLot extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            title: '',
            description: '',
            photo: [],
            price: 0,
            date: '',
            creator: this.props.creator,
            categoryId: '',
            categories: [],
            formValid: true,
            sending: false
        }

        this.findCategory = this.findCategory.bind(this);
    }
    componentWillMount(){
        $.fn.imagePicker = function( options ) {

            // Define plugin options
            var settings = $.extend({
                // Input name attribute
                name: "",
                // Classes for styling the input
                class: "btn btn-default btn-block",
                // Icon which displays in center of input
                icon: "glyphicon glyphicon-plus"
            }, options );

            // Create an input inside each matched element
            return this.each(function() {
                $(this).html(create_btn(this, settings));
            });

        };

        // Private function for creating the input element
        function create_btn(that, settings) {
            // The input icon element
            var picker_btn_icon = $('<i class="'+settings.icon+'"></i>');

            // The actual file input which stays hidden
            var picker_btn_input = $('<input type="file" name="'+settings.name+'" />');

            // The actual element displayed
            var picker_btn = $('<div class="'+settings.class+' img-upload-btn"></div>')
                .append(picker_btn_icon)
                .append(picker_btn_input);

            // File load listener
            picker_btn_input.change(function() {
                if ($(this).prop('files')[0]) {
                    // Use FileReader to get file
                    var reader = new FileReader();

                    // Create a preview once image has loaded
                    reader.onload = function(e) {
                        var preview = create_preview(that, e.target.result, settings);
                        $(that).html(preview);
                    }

                    // Load image
                    reader.readAsDataURL(picker_btn_input.prop('files')[0]);
                }
            });

            return picker_btn
        };

        // Private function for creating a preview element
        function create_preview(that, src, settings) {

            // The preview image
            var picker_preview_image = $('<img src="'+src+'" class="img-responsive img-rounded" />');

            // The remove image button
            var picker_preview_remove = $('<button class="btn btn-link"><small>Remove</small></button>');

            // The preview element
            var picker_preview = $('<div class="text-center"></div>')
                .append(picker_preview_image)
                .append(picker_preview_remove);

            // Remove image listener
            picker_preview_remove.click(function() {
                var btn = create_btn(that, settings);
                $(that).html(btn);
            });

            return picker_preview;
        };

        $(document).ready(function() {
            $('.img-picker').imagePicker({name: 'images'});
        })

        let self = this;
        let date = new Date(Date.now());
        axios.get('/api/category')
            .then((res) => {
                self.setState({
                    categories: res.data,
                    categoryId: res.data[0].title,
                    date: new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1)
                })
            }).catch((err) => {
                console.log(err);
        })
    }
    findCategory(element, index, array){
        const {categoryId} = this.state;
        if(element.title === categoryId)
            return element
    }
    sendingData(){
        let photo = [];

        $('.img-responsive').each(function(){
            photo.push($(this).attr('src'));
        });

        let {title, description, price, date,creator, formValid} = this.state;

        let category = this.state.categories.find(this.findCategory);
        let isFormValid = formValid;

        $('.form-control').each(function() {
            let formGroup = $(this).parents('.form-group');
            if (this.checkValidity() && this.value.trim() !== '') {
                formGroup.removeClass('has-error');
            } else {
                formGroup.addClass('has-error');
                isFormValid = false;
            }
        });

        if(isFormValid){
            this.setState({
                sending: true
            })
            axios.post('/api/lot/add', {
                title: title,
                description: description,
                photo: photo,
                price: parseInt(price),
                date: date,
                creator: creator,
                category: category
            }).then((res) => {
                if(res.data.message === 'Lot added successfully'){
                    browserHistory.push('/itemDetails/' + res.data.id)
                }
            }).catch((err) => {
                console.log(err)
            })
        }
    }
    onFieldChange(fieldName, e){
        let next = {};
        switch (fieldName){
            case 'date':
                next[fieldName] = e._d;
                break;
            case 'price':{
                next[fieldName] = e.target.value.trim();
                this.checkPrice(e);
            }break;
            default:
                next[fieldName] = e.target.value.trim()
        }
        this.setState(next);

    }
    onFocusDate(e){
        $('.createLot__datepicker').children('.form-control').attr("readonly","readonly");
    }
    validDate(current){
        return (current.isAfter(Datetime.moment().subtract( 0, 'day')) && current.isBefore(Datetime.moment().subtract( -30, 'day')))
    }
    checkPrice(e) {
        let value = parseInt(e.target.value.trim());
        if (value < 0 || value > 1000000000 || value % 10 > 0 || isNaN(value)) {
            $('#' + e.target.id).parent().addClass('has-error');
            this.setState({
                formValid: false
            });
        }
        else{
            $('#' + e.target.id).parent().removeClass('has-error');
            this.setState({
                formValid: true
            })
        }

    }
    render() {
        let date = new Date(Date.now());

        return (
            <div className="createLot">
                <h1>Создание лота</h1>
                <div className="createLot__info">
                   <div className="createLot__addImg">
                       <div className="createLot__mainImg">
                           <div className="img-picker"></div>
                       </div>
                       <div className="createLot__secondaryImg">
                           <div className="img-picker"></div>
                           <div className="img-picker"></div>
                           <div className="img-picker"></div>
                           <div className="img-picker"></div>
                           <div className="img-picker"></div>
                       </div>
                   </div>
                   <div className="createLot__addInfo">
                       <form className="form-horizontal" id="infoForm" encType="multipart/form-data">
                           <div className="createLot__title form-group ">
                               <label htmlFor="title"><h3>Название:</h3></label>
                               <input type="text"
                                      className="form-control"
                                      id="title"
                                      name="title"
                                      onChange={(e) => this.onFieldChange('title', e)}
                                      required="required"
                                      maxlength={128}/>
                           </div>
                           <div className="createLot__description form-group ">
                               <label htmlFor="description"><h3>Описание:</h3></label>
                               <textarea className="form-control"
                                         rows="4"
                                         id="description"
                                         onChange={(e) => this.onFieldChange('description', e)}
                                         required="required">
                               </textarea>
                           </div>
                           <div className="form-group ">
                               <label htmlFor="category"><h3>Выберите категорию:</h3></label>
                               <select className="form-control"
                                       id="category"
                                       onChange={(e) => this.onFieldChange('categoryId', e)}
                                       required="required">
                                   {this.state.categories.map(item => {
                                       return <option> {item.title}</option>
                                   })}
                               </select>
                           </div>
                           <div className="form-group ">
                               <label htmlFor="price"><h3>Минимальная сумма:</h3></label>
                               <input type="text"
                                      className="form-control"
                                      id="price"
                                      name="price"
                                      onChange={(e) => this.onFieldChange('price', e)}
                                      required="required"/>
                           </div>
                           <div className="form-group ">
                                <label htmlFor="date"><h3>Дата розыгрыша:</h3></label>
                                <Datetime className="createLot__datepicker"
                                          id="datepicker"
                                          locale="ru"
                                          closeOnSelect={true}
                                          onChange={(e) => this.onFieldChange('date', e)}
                                          value={this.state.date}
                                          onFocus={(e) => this.onFocusDate(e)} isValidDate={this.validDate}/>
                           </div>
                       </form>
                       <Button  bsSize="large" onClick={() => this.sendingData()} disabled={this.state.sending}><b><h2>Создать лот!</h2></b></Button>
                   </div>
                </div>
                
                <script>

                </script>
            </div>
        )
    }
}

/**
 * Created by Vladand on 16.06.2017.
 */
require('./app.scss');
require('./datetimepicker.scss');
require('./filepicker.css');
require('./dropzone.min.css');

import React from 'react'
import {Landing} from "./components/Landing";
import {browserHistory, IndexRoute, Route, Router} from "react-router";
import {AddMoney} from "./components/AddMoney";
import {History} from "./components/History";
import {SignUp} from "./components/SignUp";
import {LogIn} from "./components/LogIn";
import {Favorites} from "./components/Favorites";
import {ItemDetails} from "./components/ItemDetails";
import {CreateLot} from "./components/CreateLot";
import {Profile} from "./components/Profile";
import {Main} from "./components/Main";
import ReactDOM from 'react-dom'
import {App} from './components/App'

ReactDOM.render(
    <Router history={browserHistory}>
        <Route path='/' component={App}>
            <IndexRoute component={Landing} />
            <Route path='main' component={Main} />
            <Route path='profile/:id' component={Profile} />
            <Route path='createLot' component={CreateLot} />
            <Route path='itemDetails/:id' component={ItemDetails}/>
            <Route path='history' component={History} />
            <Route path='favorites' component={Favorites} />
            <Route path='login' component={LogIn} />
            <Route path='signUp' component={SignUp} />
            <Route path='addMoney' component={AddMoney} />
        </Route>
    </Router>,
    document.getElementById('root')
);

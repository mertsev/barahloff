var mongoose = require('../lib/mongoose'),
    Schema = mongoose.Schema;
var integerValidator = require('mongoose-integer');
var Category  = require('../models/category').Category;

var lotSchema = new Schema ({
    title: {
        type: String,
        text: true,
        required: [true, 'Please name your lot']
    },
    description: {
        type: String,
        required: [true, 'Please describe your lot']
    },
    photo: [String],
    price: {
        type: Number,
        integer: true,
        required: [true, 'Please add minimal selling price']
    },
    date: {
        type: Date
    },
    currentContribution: {
        type: Number,
        integer: true,
        default: 0
    },
    enoughContributed: {
        type: Boolean,
        default: false
    },
    bets: [
        {type: mongoose.Schema.ObjectId, ref: 'Bet'},
    ],
    _creator: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: [true, 'Please add lot creator']
    },
    _category: {
        type: mongoose.Schema.ObjectId,
        ref: 'Category',
        required: [true, 'Please add lot category']
    }
    ,
    _winner: {
        type: mongoose.Schema.ObjectId,
        ref: 'User'
    }
});

lotSchema.plugin(integerValidator);

exports.Lot = mongoose.model('Lot', lotSchema);
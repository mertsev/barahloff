var mongoose = require('../lib/mongoose'),
    Schema = mongoose.Schema;
var crypto = require('crypto');
var integerValidator = require('mongoose-integer');
//var mongooseIntlPhoneNumber = require('mongoose-intl-phone-number');

var userSchema = new Schema({
    email: {
        type: String,
        unique: true,
        lowercase: true
    },
    isVerified: {
        type: Boolean,
        default: false
    },
    userName: {
        type: String,
        lowercase: true
    },
    photo: {
        type: String,
        default: null
    },
    balance: {
        type: Number,
        integer: true,
        default: 0
    },
    phoneNumber: {
        type: String
    },
    vkId: {
        type: String
    },
    emailToken: {
        type: String
    },
    notifications: [{
        message: {type: String},
        link: {type: String}
    }],
    hashedPassword: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    }
});

userSchema.methods.encryptPassword = function (password) {
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

userSchema.virtual('password')
    .set(function (password) {
        this._plainPassword = password;
        this.salt = Math.random() + '';
        this.hashedPassword = this.encryptPassword(password);
    })
    .get(function () {
        return this._plainPassword;
    });

userSchema.methods.checkPassword = function (password) {
    return this.encryptPassword(password) === this.hashedPassword;
};

userSchema.plugin(integerValidator);

/*
 userSchema.plugin(mongooseIntlPhoneNumber, {
 hook: 'validate',
 phoneNumberField: 'phoneNumber',
 nationalFormatField: 'nationalFormat',
 internationalFormat: 'internationalFormat',
 countryCodeField: 'countryCode',
 });
 */

exports.User = mongoose.model('User', userSchema);
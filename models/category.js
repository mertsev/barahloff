var mongoose = require('../lib/mongoose'),
    Schema = mongoose.Schema;

var categorySchema = new Schema({
    title: {
        type: String,
        required: true,
        lowercase: true,
        unique: true
    },
    description: {
        type: String,
        required: true,
        lowercase: true
    },
    lots: [{ type: mongoose.Schema.ObjectId, ref: 'Lot' }]
});

exports.Category = mongoose.model('Category', categorySchema);

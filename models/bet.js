var mongoose = require('../lib/mongoose'),
    Schema = mongoose.Schema;
var integerValidator = require('mongoose-integer');

var betSchema = new Schema({
    userId: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'User'
    },
    amount: {
        type: Number,
        required: true,
        integer: true
    },
    lot: { type: mongoose.Schema.ObjectId, ref: 'Lot' }
});

betSchema.plugin(integerValidator);

exports.Bet = mongoose.model('Bet', betSchema);


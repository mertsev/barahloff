# README #

Barahloff - auction system.
=======
# BARAHLOFF #

Auction system.

### How to install\use ###

1. Without WebStorm
* Make sure you have node.js installed
* Clone repo to the folder of your choice
* Open powershell and change your working directory
* Set environment variables with $env:NODE_PATH="." (win) or NODE_PATH=. (bash)
* Start project with "node app.js"

2. Using WebStorm

* Open project directory

### Contribution guidelines ###

* You are not allowed to push
* All changes are made via pull requests
* Write meaningful names for pull requests\commits
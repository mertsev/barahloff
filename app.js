var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var config = require('./config');
var mongoose = require('./lib/mongoose');
var session = require('express-session');
var User = require('./models/user').User;
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var cloudinary = require('cloudinary');
var moment = require('moment');
var schedule = require('node-schedule');

var app = express();

// cloudinary configuration
cloudinary.config({
    cloud_name: 'mertsev',
    api_key: '987426385758379',
    api_secret: 'HJSZJig30TAesfuZV1GzLMg14a0'
});

// view engine setup
app.engine('ejs', require('ejs-locals'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '60mb'}));
app.use(bodyParser.urlencoded({limit: '60mb', extended: true}));
app.use(cookieParser());
var sessionStore = require('./lib/storeSession');
app.use(express.static(path.join(__dirname, 'build')));

app.use(session({
    secret: "KillerIsJim",
    key: config.get("session: key"),
    cookie: config.get("session: cookie"),
    store: sessionStore,
    resave: true,
    saveUninitialized: true
}));

app.use(require('./middleware/loadUser'));

app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, function (email, password, done) {
    User.findOne({email: email}, function (err, user) {
        return err
            ? done(err)
            : user
                ? user.checkPassword(password)
                    ? done(null, user)
                    : done(null, false, {message: 'Incorrect password.'})
                : done(null, false, {message: 'Incorrect email.'});
    });
}));

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

app.use(express.static(path.join(__dirname, 'public')));

require('./routes')(app);

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'build/index.html'))
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

var server = http.createServer(app);
server.listen(config.get('port'), function () {
    console.log('Express server listening on port ' + config.get('port'));
});